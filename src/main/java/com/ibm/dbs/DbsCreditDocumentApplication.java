package com.ibm.dbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbsCreditDocumentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbsCreditDocumentApplication.class, args);
	}
}
