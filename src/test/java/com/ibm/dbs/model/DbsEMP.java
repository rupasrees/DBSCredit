package com.ibm.dbs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="emp_dbs")
public class DbsEMP {
	
	@Id
	private String emp_id;
	private String emp_title;
	private String emp_author;
	private String submission_date;
	
	protected DbsEMP(){};
	
	
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	
	
	public String getEmp_title() {
		return emp_title;
	}
	public void setEmp_title(String emp_title) {
		this.emp_title = emp_title;
	}
	
	
	public String getEmp_author() {
		return emp_author;
	}
	public void setEmp_author(String emp_author) {
		this.emp_author = emp_author;
	}
	
	
	public String getSubmission_date() {
		return submission_date;
	}
	public void setSubmission_date(String submission_date) {
		this.submission_date = submission_date;
	}
	

}
